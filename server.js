// controleur c (controleur mbc)


var express = require('express')
var app = express()


const SPACES = [
  {
      id : 1,
      nomination : "Chambre double",
      description : "Escapade en amoureux",
      price : 35,
      detail : "Chambre pour 2 personnes"
  },
  {
      id : 2,
      nomination : "Chambre familiale",
      description : "Vacances en famille",
      price : 55 ,
      detail : "Capacité jusqu'à 5 personnes : un lit double, un lit superposé et un lit simple."
  },
  {
      id : 3,
      nomination : "Appartements",
      description : "Pour les grandes familles",
      price : 100,
      detail : "Capacité jusqu'à 10 personnes : 2 lits doubles, 2 lits superposés, 2 lits simples, une cuisine."
      
  }
  ]
  
  
  
  const FORMULAS = [
      {
          id : 1,
          designation : "week-end amour",
          discount : 10,
          bonus : "Petit déjeuner, "+'"lover"'+" servi au lit inclus !",
          condition :"Uniquement pour les chambres pour deux personnes.Cette formule applique une réduction de 10% sur le nombre total de nuitées."
      },
      {
          id : 2,
          designation : "Nature et découverte",
          discount: 5,
          bonus : "Escapade en terre du Gévaudan",
          condition : "Pour tous les types d'hebergements lorsque le nombre de nuitées est supérieur à 5 jours (weekend compris). 5% sur la totalité du séjour. Elle propose en outre, une escapade" +'"Rahan"'+" en terre du Gévaudan (le coutelas est fourni)."
      },
      {
          id : 3,
          designation : "Travail saisonnier",
          discount: 20,
          bonus : "Remise spéciale pour l'emploi saisonnier.",
          condition : "La formule"+ '"travail saisonnier"'+" applique une réduction de 20% sur la totalité du séjour (non majoré) pour tous les types d'hébergements avec une réservation de un mois minimum pour les périodes de juin à août (inclus) et de décembre à mars (inclus)."
      }
  ]
  

// app.get('/', function (req, res) {
//   res.send('hello world')
// })

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get('/spaces', function(req, res){
    res.setHeader('Access-Control-Allow-Origin',"*");
    // res.setHeader('Content-Type', "aplication/json");
    res.json(SPACES);
});


app.get('/spaces/:id', function(req,res){
    res.setHeader('Access-Control-Allow-Origin',"*");
    // res.setHeader('Content-Type', "aplication/json");
    res.json(SPACES);
})

// 

app.get('/formulas', function(req, res){
    res.setHeader('Access-Control-Allow-Origin','*');
    // res.setHeader('Content-Type', "aplication/json");
    res.json(FORMULAS);
});

app.get('/formulas/:id', function(req,res){
    res.setHeader('Access-Control-Allow-Origin','*');
    // res.setHeader('Content-Type', "aplication/json");
    res.json(FORMULAS);
})

app.post('/formulas', function(req,res){ 
    SPACES.push(req.body);
    res.status(202).send("ok"); 
})



app.post('/spaces', function(req,res){
    SPACES.push(req.body);
    res.status(202).send("ok");
})

app.post('/space', function(req,res){
    req.body.price = parseInt(req.body.price);
    SPACES.push(req.body);
    res.status(202).send("ok");
})

app.listen(7777) //web server

//app.listen : Lie et écoute les connexions sur l'hôte et le port spécifiés.








